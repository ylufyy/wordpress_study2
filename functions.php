<?php
function get_css(){
    //引入js
    wp_enqueue_script('main_js',get_theme_file_uri('/build/index.js'),array('jquery'),'1.0.1',true);
   //引入样式
    wp_enqueue_style('custtom-google-fonts','//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
    // wp_enqueue_style('font-awesome','//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('extra_style',get_theme_file_uri('/build/style-index.css'));
    wp_enqueue_style('main_style',get_theme_file_uri('/build/index.css'));
    wp_enqueue_style('test', get_stylesheet_uri());
}
function university_features(){
    //注册菜单
    register_nav_menu('headerMenuLocation','Header Menu Location');
    add_theme_support('title-tag');
}
//在加载script文件时，get_css()函数
add_action('wp_enqueue_scripts', 'get_css');
add_action('after_setup_theme','university_features');
?>