<?php 
get_header();?>
<div class="page-banner">
      <div class="page-banner__bg-image" style="background-image: url(<?php echo get_theme_file_uri('/images/ocean.jpg') ?>)"></div>
      <div class="page-banner__content container t-center c-white">
        <h1 class="headline headline--large">欢迎来到我的博客</h1>
        <a href="#" class="btn btn--large btn--blue">Find Your Major</a>
      </div>
</div>
<div class="container container--narrow page-section">
  <?php while(have_posts()){
    the_post();
    ?>
    <div class="post-item">
    <h2><a href="<?php the_permalink(); ?>"><?php the_title()?></a></h2></div>
      <div class="metabox">
          <p>作者 <?php the_author_posts_link(); ?> 发布于 <?php the_time('Y.m.d'); ?>  <?php echo get_the_category_list(', '); ?></p>
      </div>

    <div class="generic-content">
        <?php the_excerpt(); ?>
        <p><a class="btn btn--blue" href="<?php the_permalink(); ?>">继续阅读</a></p>
    </div>
  <?php } ?>
</div>
<?php get_footer(); ?>